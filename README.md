# Rash
A Ruby Interactive Shell (Rash instead of Bash)

## Installation

For now you have to checkout this repository.
Until a v. 1.0 is released to rubygems.

## Usage

```bash
bundle exec bin/rash
```

Once run try a few things:

```ruby
cd '/tmp'
@a = /bin/ls('-al', '.')
2 + 2
ps('-ef')
```

Note: This is still very much a prototype I did cause I could not sleep.

## Contributing

0. Create an issue on github.
1. Fork it ( https://github.com/bloodycelt/rash/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
