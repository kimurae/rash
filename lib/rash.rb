require 'coolline'
require 'coderay'
require 'pp'
require 'thor'

require 'rash/shell_builtins'
require 'rash/shell_evaluation'
require 'rash/version'

# Required in a specific order.
require 'rash/shell'
require 'rash/application'

module Rash; end

