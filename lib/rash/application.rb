module Rash
  class Application < ::Thor

    desc 'rash', 'Run an interactive shell'
    def rash
      Rash::Shell.start
    end

    default_task :rash
  end
end
