module Rash
  class Shell
    include ShellBuiltins
    include ShellEvaluation

    attr_accessor :prompt

    def self.start
      self.new.repl
    end

    def initialize
      @prompt = Proc.new { '=> ' }

      instance_eval rashrc

      super
    end

    # Read - Eval - Print - Loop
    def repl

      loop do

        response = begin
          evaluate(coolline.readline(prompt.call))
        rescue => e
          e
        end

        case response
        when String; puts response
        else
          print '=> '
          pp    response
        end

      end
    end

    private

    def coolline
      @coolline ||= Coolline.new do |c|

        # Before the line is displayed, it gets passed through this proc,
        # which performs syntax highlighting.
        c.transform_proc = proc do
          CodeRay.scan( c.line, :ruby).term
        end

        # Add tab completion for constants (and classes)
        c.completion_proc = proc do
          word = c.completed_word
          Object.constants.map(&:to_s).select { |w| w.start_with? word } +
          Dir["#{word}*"]
        end
      end
    end

    def rashrc
      path = File.join(ENV['HOME'],'.rashrc')

      if File.file?(path)
        File.read(path)
      else
        'nil'
      end
    end

  end
end

