module Rash
  module ShellBuiltins

    # Changes directory
    # returns - String: the directory path.
    def cd(dir)
      Dir.chdir(dir.to_s)
      Dir.pwd
    end

    # Sends a signal to a process.
    # defaults to KILL like normal kill.
    def kill(pid, signal = 15)
      Process.kill(signal, pid)
    end

    # Exits.
    def quit
      Kernel.exit
    end
  end
end
