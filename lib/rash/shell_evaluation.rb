require 'coolline'
require 'coderay'
require 'pp'

module Rash
  module ShellEvaluation

    def evaluate(line)
      meth, *args = line.split

      unless meth.nil?
        if respond_to?(meth.to_sym)
          send(meth, *args.map { |a| instance_eval(a) })
        else
          instance_eval(sanitized_line(line))
        end
      end
    end

    private

    def execute(meth, *args)
      r, w = IO.pipe
      begin
        pid = spawn(ENV, meth.to_s, *args, :out => w, :err => w, :chdir => Dir.pwd)
        puts "Executing #{meth} with #{[*args]} at: #{pid}"
        Process.wait(pid)
        r
      rescue => e
        r.close
        raise e
      ensure
        w.close
      end.read
    end

    def method_missing(meth, *args, &block)
      execute(meth, *args)
    end


    def sanitized_line(line)
      line.gsub(%r{(/.*?)\((.*?)\)}, 'send(:"\1", \2)').split.map do |atom|
        if atom =~ %r{/} && atom !~ %r{^\w+\(}
          "send(:'#{atom}')"
        else
          atom
        end
      end.join
    end

  end
end
