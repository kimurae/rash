# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rash/version'

Gem::Specification.new do |spec|
  spec.name          = 'rash'
  spec.version       = Rash::VERSION
  spec.authors       = ['Jason Kenney']
  spec.email         = ['bloodycelt@gmail.com']
  spec.summary       = %q{An Interactive Ruby Shell}
  spec.description   = %q{Write a longer description. Optional.}
  spec.homepage      = %q{https://github.com/bloodycelt/rash}
  spec.license       = %q{MIT}

  spec.files         = %W(
    LICENSE.txt
    README.md
    lib/rash.rb
    lib/rash/application.rb
    lib/rash/shell.rb
    lib/rash/shell_builtins.rb
    lib/rash/shell_evaluation.rb
    lib/rash/version.rb
    rash.gemspec
  )

  spec.executables   = []
  spec.test_files    = %W(
    spec/lib/rash/shell_builtins_spec.rb
    spec/lib/rash/shell_evaluation_spec.rb
    spec/spec_helper.rb
  )
  spec.require_paths = ['lib']

  spec.add_dependency 'coolline', '~> 0.4'
  spec.add_dependency 'coderay',  '~> 1.1'
  spec.add_dependency 'thor',     '~> 0.19'

  spec.add_development_dependency 'bundler', '~> 1.6'
  spec.add_development_dependency 'rake',    '~> 10'
  spec.add_development_dependency 'rspec', '~> 3.0.0.beta2'
end
