require 'spec_helper'

RSpec.describe Rash::ShellBuiltins do

  let(:shell) do
    Class.new do
      include Rash::ShellBuiltins
    end.new
  end

  context '#cd' do
    subject { shell.cd('/') }

    it { is_expected.to eq '/' }

    it 'should modify the current PWD' do
      subject
      expect(`pwd`).to eq "/\n"
    end
  end

  context '#quit' do
    subject { shell }
    it 'should quit when called' do
      expect(Kernel).to receive(:exit)
      subject.quit
    end
  end

  pending 'alias'
  pending 'bg'
  pending 'dirs'
  pending 'disown'
  pending 'echo'
  pending 'export'
  pending 'fg'
  pending 'history'
  pending 'jobs'
  describe '#kill' do
    subject { $?.exitstatus }
    let(:pid) do
      fork do
        Signal.trap('TERM') { exit 3 }
        loop { sleep 1 }
      end
    end
    it 'should kill a process when called' do
      shell.kill(pid)
      Process.wait(pid)
      is_expected.to eq 3
    end
  end
  pending 'limit'
  pending 'log'
  pending 'popd'
  pending 'pushd'
  pending 'pwd'
  pending 'set'
  pending 'suspend'
  pending 'times'
  pending 'typeset'
  pending 'ulimit'
  pending 'umask'
  pending 'unalias'
  pending 'unlimit'
  pending 'unset'
  pending 'wait'
end
