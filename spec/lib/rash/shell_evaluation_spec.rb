require 'spec_helper'

RSpec.describe Rash::ShellEvaluation do

  let(:shell) do
    Class.new do
      include Rash::ShellEvaluation

      def test_method(meh)
        42
      end
    end.new
  end

  context '#/bin/ls' do
    let(:output) { `/bin/ls -la .`}

    subject { shell.evaluate(%q{/bin/ls('-la', '.') }) }

    it { is_expected.to eq output }

  end

  context '#test_method' do
    subject { shell.evaluate(%q{ test_method 32}) }

    it { is_expected.to eq 42 }
  end

  context '#instance_eval' do
    subject { shell.evaluate('2 + 2') }

    it { is_expected.to eq 4 }
  end
end
